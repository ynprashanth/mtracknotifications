package in.fortelogic.mtracknotifications.util.TextUtil;

import android.content.Context;
import android.graphics.Typeface;

/*MediumTextView is custom TextView created using android TextView with custom type face*/
public class MediumTextView extends android.support.v7.widget.AppCompatTextView
{
    public MediumTextView(Context context)
    {
        super(context);
        applyCustomFont();
    }
    //Method to change typeface style
    private void applyCustomFont()
    {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/EncodeSans-Medium.ttf");
        setTypeface(tf, 1);
    }

}
