package in.fortelogic.mtracknotifications.util.TextUtil;

import android.content.Context;
import android.graphics.Typeface;

/*BoldTextView is custom TextView created using android TextView with custom typeface*/
public class BoldTextView extends android.support.v7.widget.AppCompatTextView
{
    public BoldTextView(Context context)
    {
        super(context);
        applyCustomFont();
    }
    //Method to change typeface style
    private void applyCustomFont()
    {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/EncodeSans-Bold.ttf");
        setTypeface(tf, 1);
    }

}
