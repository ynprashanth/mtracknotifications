package in.fortelogic.mtracknotifications.util.TextUtil;

import android.content.Context;
import android.graphics.Typeface;

/*ExtraBoldTextView is custom TextView created using android TextView with custom type face*/
public class ExtraBoldTextView extends android.support.v7.widget.AppCompatTextView
{
    public ExtraBoldTextView(Context context)
    {
        super(context);
        applyCustomFont();
    }
    //Method to change typeface style
    private void applyCustomFont()
    {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "font/EncodeSans-ExtraBold.ttf");
        setTypeface(tf, 1);
    }

}
